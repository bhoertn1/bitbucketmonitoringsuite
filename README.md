# Bitbucket Monitoring Suite

Bitbucket Monitoring Suite is a set of tools that support open source practices at BlackRock.

## Features
* BlackRock Open Source Software (OSS) Scanner - notification of missing components, suggested documentation update
* Dormant Repository Janitor - flag uninitialized for removal and deprecated for update 
* Overlapping Project Detector -suggest merge/porting of similar projects

## Want to help?

Want to file a bug, contribute some code, or improve documentation? Excellent! Read up on our
guidelines for contributing!