import requests
import json
import os

out_dir = 'out/'
check = ['README.md', 'CHANGELOG.md', 'CONTRIBUTING.md']
def readFile(projectKey, repoName, file):
    flags = filter(lambda x: x in check, [path for values in file['children']['values'] for path in values['path']['components']] )	
    with open(os.path.join(out_dir, repoName + '.json'), 'w') as outfile:
        json.dump(file, outfile)

def request(url, choice='json'):
    headers = {'Content-Type': 'application/json'}
    response = requests.get(url, auth=('[USERNAME]', '[DEV-PASSWORD]'), headers=headers)
    if choice == 'json':
        data = response.json()
    elif choice == 'text':
        data = response.text
    return data


if __name__ == "__main__":
    projData = request('https://[MainPage]/rest/api/1.0/projects?limit=1000')
    for project in projData['values']:
        projectKey = project['key']
        print("Search in Project[" + projectKey + "]...")
        repoData = request('https://[MainPage]/rest/api/1.0/projects/' + projectKey + '/repos?limit=1000')
        for repo in repoData['values']:
            repoName = repo['slug']
            print("----Found Repo[" + repoName + "]...")
            components = request('https://[MainPage]/rest/api/1.0/projects/' + projectKey + '/repos/' + repoName + '/browse')
            try:
                error = components['path']
            except:
                print("--------has not been initialized.")
                continue
            readFile(projectKey, repoName, components)