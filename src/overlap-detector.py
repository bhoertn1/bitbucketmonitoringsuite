import xmltodict
import json

mapping = []

def readFile(projectKey, repoName, file):
    flags = filter(lambda x: x in check, [path for values in file['children']['values'] for path in values['path']['components']] ) 
    if valid(projectKey, repoName, flags):
        with open(os.path.join(out_dir, repoName + '.json'), 'w') as outfile:
            json.dump(file, outfile)

def ordered(obj):
    if isinstance(obj, dict):
        return sorted((k, ordered(v)) for k, v in obj.items())
    if isinstance(obj, list):
        return sorted(ordered(x) for x in obj)
    else:
        return obj

def readPom(repoName):
    with open(repoName + '.json') as fd:
        doc = xmltodict.parse(fd.read())
    doc = ordered(doc)[0][1]
    dependencies = doc[5][1][0][1]
    return {dep[0][1] for dep in dependencies}

def similarity(set1, set2):
    return abs(len(set1)-len(set2))/ (len(set1)+len(set2))

def request(url, choice='json'):
    headers = {'Content-Type': 'application/json'}
    response = requests.get(url, auth=('[USERNAME]', '[DEV-PASSWORD]'), headers=headers)
    if choice == 'json':
        data = response.json()
    elif choice == 'text':
        data = response.text
    return data


if __name__ == "__main__":
    projData = request('https://[MainPage]/rest/api/1.0/projects?limit=1000')
    for project in projData['values']:
        projectKey = project['key']
        print("Search in Project[" + projectKey + "]...")
        repoData = request('https://[MainPage]/rest/api/1.0/projects/' + projectKey + '/repos?limit=1000')
        for repo in repoData['values']:
            repoName = repo['slug']
            print("----Found Repo[" + repoName + "]...")
            components = request('https://[MainPage]/rest/api/1.0/projects/' + projectKey + '/repos/' + repoName + '/browse')
            try:
                error = components['path']
                components = request('https://[MainPage]/rest/api/1.0/projects/' + projectKey + '/repos/' + repoName + '/browse/pom.xml')
            except:
                print("--------has not been initialized.")
                continue
            readFile(projectKey, repoName, components)